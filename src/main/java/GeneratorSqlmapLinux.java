/**
 * Copyright 2006-2022 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//import com.ctl.po.TbTest;
import com.ctl.po.TbTest;
import com.ctl.util.SnowflakeIdUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>Title: run</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: https://gitee.com/ctllin</p>
 *
 * @author ctl
 * @version 1.0
 * @date 2022-04-01 10:09
 */
public class GeneratorSqlmapLinux {
    static Logger logger = LoggerFactory.getLogger(GeneratorSqlmapLinux.class);
    // http://mybatis.org/generator/configreference/xmlconfig.html   #xml配置
    // https://github.com/mybatis/generator/archive/mybatis-generator-1.4.1.zip
    public void generator() throws Exception {
        List<String> warnings = new ArrayList<>();
        boolean overwrite = true;
        File configFile = new File(GeneratorSqlmapLinux.class.getResource("generatorConfigLinux.xml").getPath());
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
//        List<GeneratedJavaFile> generatedJavaFiles = myBatisGenerator.getGeneratedJavaFiles();
//        List<GeneratedXmlFile> generatedXmlFiles = myBatisGenerator.getGeneratedXmlFiles();
//        logger.info("java files:" + generatedJavaFiles);
//        logger.info("xml files:" + generatedXmlFiles);
    }

    public static void main(String[] args) throws Exception {
        try {
            GeneratorSqlmapLinux generatorSqlmap = new GeneratorSqlmapLinux();
            generatorSqlmap.generator();

            SqlSession sqlSession = getSqlSession();
            sqlSession.commit(true);
            com.ctl.mapper.TbTestMapper mapper = sqlSession.getMapper(com.ctl.mapper.TbTestMapper.class);
            logger.info("{}",mapper.selectByPrimaryKey(1L));
            com.ctl.po.TbTestExample tbTestExample = new com.ctl.po.TbTestExample();
            tbTestExample.createCriteria().andIdGreaterThan(0L);
            tbTestExample.setOffset(0);
            tbTestExample.setLimit(10);
            logger.info("{}",mapper.selectByExample(tbTestExample));
            List<com.ctl.po.TbTest> list = new ArrayList<>();
            for (int i = 0; i < 30; i++) {
                com.ctl.po.TbTest tbTest = new com.ctl.po.TbTest();
                tbTest.setId(SnowflakeIdUtils.genID());
                tbTest.setSource(new Random().nextInt(999999)+1);
                tbTest.setVar(StringUtils.leftPad("0",new Random().nextInt(999999)+1));
                tbTest.setDefaultAddress(StringUtils.leftPad("0",6,"xx"+new Random().nextInt(9999)));
                list.add(tbTest);
                if(list.size()%10==0){
                    mapper.batchInsert(list);
                    list.clear();
                }
            }
            for (int i = 0; i < 30; i++) {
                com.ctl.po.TbTest tbTest = new com.ctl.po.TbTest();
                tbTest.setId(SnowflakeIdUtils.genID());
                tbTest.setSource(new Random().nextInt(999999)+1);
                tbTest.setVar(StringUtils.leftPad("0",new Random().nextInt(999999)+1));
                tbTest.setDefaultAddress(StringUtils.leftPad("0",6,"xx"+new Random().nextInt(9999)));
                list.add(tbTest);
                if(list.size()%10==0){
                    mapper.batchInsertSelective(list, TbTest.Columns.ID, TbTest.Columns.SOURCE, TbTest.Columns.DEFAULT_ADDRESS);
                    list.clear();
                }
            }
            sqlSession.commit();
            sqlSession.close();

        } catch (Exception e) {
            logger.error("",e);
        }
    }
    private static SqlSessionFactory sessionFactory;

    public static SqlSession getSqlSession() {
        try {
            // 流关闭了吗？？
            InputStream iStream = Resources.getResourceAsStream("mybatis.xml");
            if (sessionFactory == null) {
                sessionFactory = new SqlSessionFactoryBuilder().build(iStream);
            }
            return sessionFactory.openSession();
        } catch (IOException e) {
            logger.error("",e);
        }
        return null;
    }

}
