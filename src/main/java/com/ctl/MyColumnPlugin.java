package com.ctl;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.codegen.mybatis3.ListUtilities;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * <p>Description: public enum Columns {
 *         ID("id"),
 *         SOURCE("source"),
 *         VAR("var"),
 *         DEFAULT_ADDRESS("default_address"),
 *         CONTENT("content");
 *
 *         private String column;
 *
 *         Columns(String column) {
 *             this.column=column;
 *         }
 *
 *         public void setColumn(String column) {
 *             this.column = column;
 *         }
 *
 *         public String getColumn() {
 *             return column;
 *         }
 *     }</p>
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: https://gitee.com/ctllin</p>
 * @author ctl
 * @version 1.0
 * @date 2022-04-01 10:09
 */
public class MyColumnPlugin extends PluginAdapter {
    public static final String ENUM_NAME = "Columns";
    private boolean useColumnRoot;

    public MyColumnPlugin() {
    }

    public void setProperties(Properties properties) {
        super.setProperties(properties);
        this.useColumnRoot = StringUtility.isTrue(properties.getProperty("useColumnRoot"));
    }

    public boolean validate(List<String> warnings) {
        return true;
    }

    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        this.generateToString(introspectedTable, topLevelClass);
        return true;
    }

    public boolean modelRecordWithBLOBsClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        this.generateToString(introspectedTable, topLevelClass);
        return true;
    }

    public boolean modelPrimaryKeyClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        this.generateToString(introspectedTable, topLevelClass);
        return true;
    }

    private void generateToString(IntrospectedTable introspectedTable, TopLevelClass topLevelClass) {
        String columnUp = "Column";
        String column = "column";
        FullyQualifiedJavaType fullyQualifiedJavaType = new FullyQualifiedJavaType(ENUM_NAME);
        InnerEnum innerEnum = new InnerEnum(fullyQualifiedJavaType);
        innerEnum.setVisibility(JavaVisibility.PUBLIC);
        List<IntrospectedColumn> introspectedColumns = ListUtilities.removeIdentityAndGeneratedAlwaysColumns(introspectedTable.getAllColumns());
        Iterator var = introspectedColumns.iterator();
        while (var.hasNext()) {
            IntrospectedColumn introspectedColumn = (IntrospectedColumn) var.next();
            innerEnum.getEnumConstants().add(introspectedColumn.getActualColumnName().toUpperCase() + "(\"" + introspectedColumn.getActualColumnName() + "\")");
        }
        Field field = new Field(column, FullyQualifiedJavaType.getStringInstance());
        field.setName(column);
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getStringInstance());
        innerEnum.addField(field);

        Method method = new Method(ENUM_NAME);
        method.setConstructor(true);
        method.setVisibility(JavaVisibility.DEFAULT);
        method.setReturnType(fullyQualifiedJavaType);
        method.addBodyLine("this." + column + "=" + column + ";");
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), column));
        innerEnum.addMethod(method);


        Method setColumn = new Method("set" + columnUp);
        setColumn.setName("set" + columnUp);
        setColumn.setVisibility(JavaVisibility.PUBLIC);
        setColumn.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), column));
        setColumn.addBodyLine("this." + column + " = " + column + ";");
        innerEnum.addMethod(setColumn);

        Method getColumn = new Method("get" + columnUp);
        getColumn.setName("get" + columnUp);
        getColumn.setVisibility(JavaVisibility.PUBLIC);
        getColumn.setReturnType(FullyQualifiedJavaType.getStringInstance());
        getColumn.addBodyLine("return " + column + ";");
        innerEnum.addMethod(getColumn);

        topLevelClass.addInnerEnum(innerEnum);
    }
}