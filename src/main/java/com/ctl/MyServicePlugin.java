package com.ctl;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.config.PropertyRegistry;

import java.util.*;

import static com.ctl.utils.PluginUtils.primaryKeyType;


/**
 * <p>Title: run</p>
 * <p>Description: 自定义service插件</p>
 * <p>Copyright: Copyright (c) 2022</p>
 * <p>Company: https://gitee.com/ctllin</p>
 *
 * @author ctl
 * @version 1.0
 * @date 2022-04-01 10:09
 */
public class MyServicePlugin extends PluginAdapter {

    private String targetProject = null;

    private String targetPackage = null;

    private String basicService = null;

    private String basicServiceImpl = null;

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public void setProperties(Properties properties) {
        super.setProperties(properties);
        targetProject = properties.getProperty("targetProject");
        targetPackage = properties.getProperty("targetPackage");
        basicService = properties.getProperty("basicService");
        basicServiceImpl = properties.getProperty("basicServiceImpl");
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
        if (Objects.nonNull(targetPackage) && Objects.nonNull(targetProject)) {
            List<GeneratedJavaFile> files = new ArrayList<>();
            //Get entity class name
            String recordType = introspectedTable.getBaseRecordType();
            String[] entityPackage = recordType.split("\\.");
            String className = entityPackage[entityPackage.length - 1];
            //Create interface
            String servicePackage = targetPackage + "." + className + "Service";
            FullyQualifiedJavaType service = new FullyQualifiedJavaType(servicePackage);
            Interface serviceInterface = new Interface(service);
            serviceInterface.setVisibility(JavaVisibility.PUBLIC);
            if (Objects.nonNull(basicService)) {
                //Import package
                serviceInterface.addImportedType(new FullyQualifiedJavaType(basicService));
                //Interface name
                String[] basicServicePackage = basicService.split("\\.");
                FullyQualifiedJavaType interfacePackage = new FullyQualifiedJavaType(basicServicePackage[basicServicePackage.length - 1]);
                //接口添加泛型格式BaseService<实体,主键类型>
                serviceInterface.addImportedType(new FullyQualifiedJavaType(recordType));
                interfacePackage.addTypeArgument(new FullyQualifiedJavaType(recordType));
                Optional<FullyQualifiedJavaType> optional = primaryKeyType(introspectedTable);
                if (optional.isPresent()) {
                    FullyQualifiedJavaType javaType = optional.get();
                    if (javaType.isExplicitlyImported()) {
                        serviceInterface.addImportedType(javaType);
                    }
                    interfacePackage.addTypeArgument(javaType);
                }
                serviceInterface.addSuperInterface(interfacePackage);
                GeneratedJavaFile javaFile = new GeneratedJavaFile(serviceInterface, targetProject,
                        context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
                        context.getJavaFormatter());
                files.add(javaFile);
            }
            if (Objects.nonNull(basicServiceImpl) && Objects.nonNull(basicService)) {
                String[] serviceImplPackage = basicServiceImpl.split("\\.");
                //Create an implementation class interface
                FullyQualifiedJavaType serviceImpl = new FullyQualifiedJavaType(targetPackage + "." + "impl." + className + "ServiceImpl");
                TopLevelClass serviceImplClass = new TopLevelClass(serviceImpl);
                serviceImplClass.setVisibility(JavaVisibility.PUBLIC);
                serviceImplClass.addImportedType("org.springframework.stereotype.Service");
                serviceImplClass.addImportedType("org.slf4j.LoggerFactory");
                serviceImplClass.addImportedType("org.slf4j.Logger");
                serviceImplClass.addAnnotation("@Service");
                //Inherit Base Service Impl
                serviceImplClass.addImportedType(basicServiceImpl);
                FullyQualifiedJavaType javaType = new FullyQualifiedJavaType(serviceImplPackage[serviceImplPackage.length - 1]);
                serviceImplClass.addImportedType(recordType);
                javaType.addTypeArgument(new FullyQualifiedJavaType(recordType));
                Optional<FullyQualifiedJavaType> optional = primaryKeyType(introspectedTable);
                if (optional.isPresent()) {
                    FullyQualifiedJavaType qualifiedJavaType = optional.get();
                    if (qualifiedJavaType.isExplicitlyImported()) {
                        //Not the basic data type needs to guide package
                        serviceImplClass.addImportedType(qualifiedJavaType);
                    }
                    javaType.addTypeArgument(qualifiedJavaType);
                }
                serviceImplClass.setSuperClass(javaType);
                //Add implementation class
                serviceImplClass.addImportedType(servicePackage);
                serviceImplClass.addSuperInterface(new FullyQualifiedJavaType(className + "Service"));
                Field loggerField = new Field("logger",new FullyQualifiedJavaType("org.slf4j.Logger"));
                loggerField.setVisibility(JavaVisibility.PRIVATE);
                loggerField.setStatic(true);
                loggerField.setFinal(true);
                String[] split = introspectedTable.getBaseRecordType().split("\\.");
                String name=split[split.length-1]+"ServiceImpl.class";
                loggerField.setInitializationString("LoggerFactory.getLogger("+name+")");
                serviceImplClass.addField(loggerField);
                GeneratedJavaFile javaFile = new GeneratedJavaFile(serviceImplClass, targetProject,
                        context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
                        context.getJavaFormatter());
                files.add(javaFile);
            }
            return files;
        }
        return Collections.emptyList();
    }


}
