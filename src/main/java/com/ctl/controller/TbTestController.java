package com.ctl.controller;

import com.ctl.dto.ReponseDto;
import com.ctl.po.TbTest;
import com.ctl.service.TbTestService;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("tbTest")
public class TbTestController {
    private static final Logger logger = LoggerFactory.getLogger(TbTestController.class);


    @Autowired
    private TbTestService tbTestService;

    @GetMapping("findAll")
    public ReponseDto findAll() {
        return  null;
    }

    @GetMapping("findById/{id}")
    public ReponseDto findById(@PathVariable Long id) {
        return  null;
    }

    @PostMapping("save")
    public ReponseDto save(@RequestBody TbTest record) {
        return  null;
    }

    @PostMapping("saveBatch")
    public ReponseDto saveBatch(@RequestBody List<TbTest> records) {
        return  null;
    }

    @PutMapping("update")
    public ReponseDto update(@RequestBody TbTest record) {
        return  null;
    }

    @PatchMapping("updateBatch")
    public ReponseDto updateBatch(@RequestBody List<TbTest> records) {
        return  null;
    }

    @DeleteMapping("delete/{id}")
    public ReponseDto delete(@PathVariable Long id) {
        return  null;
    }

    @DeleteMapping("deleteBatch/{ids}")
    public ReponseDto deleteBatch(@PathVariable Long [] ids) {
        return  null;
    }

    @GetMapping("page")
    public ReponseDto page(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "6") int pageSize) {
        return  null;
    }
}