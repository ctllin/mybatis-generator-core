package com.ctl.mapper;

import com.ctl.po.TbTest;
import com.ctl.po.TbTestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbTestMapper {
    long countByExample(TbTestExample example);

    int deleteByExample(TbTestExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbTest row);

    int insertSelective(TbTest row);

    List<TbTest> selectByExampleWithBLOBs(TbTestExample example);

    List<TbTest> selectByExample(TbTestExample example);

    TbTest selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("row") TbTest row, @Param("example") TbTestExample example);

    int updateByExampleWithBLOBs(@Param("row") TbTest row, @Param("example") TbTestExample example);

    int updateByExample(@Param("row") TbTest row, @Param("example") TbTestExample example);

    int updateByPrimaryKeySelective(TbTest row);

    int updateByPrimaryKeyWithBLOBs(TbTest row);

    int updateByPrimaryKey(TbTest row);

    int batchInsertSelective(@Param("list") List<TbTest> list, @Param("selective") TbTest.Columns ... selective);

    int batchInsert(@Param("list") List<TbTest> list);
}