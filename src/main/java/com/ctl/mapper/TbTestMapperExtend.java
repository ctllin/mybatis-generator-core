package com.ctl.mapper;

import com.ctl.po.TbTest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbTestMapperExtend extends TbTestMapper {
    int batchInsertSelectiveExtend(@Param("list") List<TbTest> list, @Param("selective") TbTest.Columns ... selective);

    int batchInsertExtend(@Param("list") List<TbTest> list);
}