package com.ctl.po;

import java.util.ArrayList;
import java.util.List;

public class TbTestExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public TbTestExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSourceIsNull() {
            addCriterion("source is null");
            return (Criteria) this;
        }

        public Criteria andSourceIsNotNull() {
            addCriterion("source is not null");
            return (Criteria) this;
        }

        public Criteria andSourceEqualTo(Integer value) {
            addCriterion("source =", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotEqualTo(Integer value) {
            addCriterion("source <>", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThan(Integer value) {
            addCriterion("source >", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThanOrEqualTo(Integer value) {
            addCriterion("source >=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThan(Integer value) {
            addCriterion("source <", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThanOrEqualTo(Integer value) {
            addCriterion("source <=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceIn(List<Integer> values) {
            addCriterion("source in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotIn(List<Integer> values) {
            addCriterion("source not in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceBetween(Integer value1, Integer value2) {
            addCriterion("source between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotBetween(Integer value1, Integer value2) {
            addCriterion("source not between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andVarIsNull() {
            addCriterion("var is null");
            return (Criteria) this;
        }

        public Criteria andVarIsNotNull() {
            addCriterion("var is not null");
            return (Criteria) this;
        }

        public Criteria andVarEqualTo(String value) {
            addCriterion("var =", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarNotEqualTo(String value) {
            addCriterion("var <>", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarGreaterThan(String value) {
            addCriterion("var >", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarGreaterThanOrEqualTo(String value) {
            addCriterion("var >=", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarLessThan(String value) {
            addCriterion("var <", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarLessThanOrEqualTo(String value) {
            addCriterion("var <=", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarLike(String value) {
            addCriterion("var like", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarNotLike(String value) {
            addCriterion("var not like", value, "var");
            return (Criteria) this;
        }

        public Criteria andVarIn(List<String> values) {
            addCriterion("var in", values, "var");
            return (Criteria) this;
        }

        public Criteria andVarNotIn(List<String> values) {
            addCriterion("var not in", values, "var");
            return (Criteria) this;
        }

        public Criteria andVarBetween(String value1, String value2) {
            addCriterion("var between", value1, value2, "var");
            return (Criteria) this;
        }

        public Criteria andVarNotBetween(String value1, String value2) {
            addCriterion("var not between", value1, value2, "var");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressIsNull() {
            addCriterion("default_address is null");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressIsNotNull() {
            addCriterion("default_address is not null");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressEqualTo(String value) {
            addCriterion("default_address =", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressNotEqualTo(String value) {
            addCriterion("default_address <>", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressGreaterThan(String value) {
            addCriterion("default_address >", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressGreaterThanOrEqualTo(String value) {
            addCriterion("default_address >=", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressLessThan(String value) {
            addCriterion("default_address <", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressLessThanOrEqualTo(String value) {
            addCriterion("default_address <=", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressLike(String value) {
            addCriterion("default_address like", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressNotLike(String value) {
            addCriterion("default_address not like", value, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressIn(List<String> values) {
            addCriterion("default_address in", values, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressNotIn(List<String> values) {
            addCriterion("default_address not in", values, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressBetween(String value1, String value2) {
            addCriterion("default_address between", value1, value2, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressNotBetween(String value1, String value2) {
            addCriterion("default_address not between", value1, value2, "defaultAddress");
            return (Criteria) this;
        }

        public Criteria andVarLikeInsensitive(String value) {
            addCriterion("upper(var) like", value.toUpperCase(), "var");
            return (Criteria) this;
        }

        public Criteria andDefaultAddressLikeInsensitive(String value) {
            addCriterion("upper(default_address) like", value.toUpperCase(), "defaultAddress");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}