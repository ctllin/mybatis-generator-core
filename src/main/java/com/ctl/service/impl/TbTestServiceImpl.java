package com.ctl.service.impl;

import com.ctl.po.TbTest;
import com.ctl.service.TbTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TbTestServiceImpl extends BaseServiceImpl<TbTest, Long> implements TbTestService {
    private static final Logger logger = LoggerFactory.getLogger(TbTestServiceImpl.class);
}