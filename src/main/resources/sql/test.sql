/*
 Navicat Premium Data Transfer

 Source Server         : 10.11.185.31 @@
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 10.11.185.31:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 31/03/2022 15:02:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_test
-- ----------------------------
DROP TABLE IF EXISTS `tb_test`;
CREATE TABLE `tb_test`
(
  `id`              BIGINT      NOT NULL AUTO_INCREMENT,
  `source`          INT         NULL DEFAULT NULL,
  `var`             CHAR(6)     NULL DEFAULT NULL,
  `content`         LONGTEXT    NULL,
  `default_address` VARCHAR(64) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sku` (`var`) USING BTREE
) COLLATE = 'utf8_general_ci'
  ENGINE = InnoDB
  ROW_FORMAT = DYNAMIC
  AUTO_INCREMENT = 0;

-- ----------------------------
-- Records of tb_test
-- ----------------------------
INSERT INTO `tb_test` VALUES (1, 1, '123456','t1','xx');
INSERT INTO `tb_test` VALUES (2, 2, '123456','t2','xx');
INSERT INTO `tb_test` VALUES (3, 3, '123456','t3','xx');

SET FOREIGN_KEY_CHECKS = 1;
